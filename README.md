# OpenbisTools


This respository holds a collection of tools for [openBIS](https://openbis.ch/) that have been developed at the Institute for Physical Metallurgy and Materials Physics [IMM](https://www.imm.rwth-aachen.de/index.php?id=2&L=1)

# Dropboxes

The institute has a number of scientific instruments that can be integrated into openBIS using the dropbox feature of openBIS.

The following instruments are currently supported

- Tescan Clara (SEM)
- FEI Helios (SEM)
- Zeiss Leo (SEM)

# Metadata Extractors
The repository contains a number of small python scripts that can parse various file formats and extract metadata.
If we can make the parser compatible with Python 2.7 (standard libary), we can build a dropbox. Unfortunately, a number of formats require packages that are not available in Python 2.7 and the standard library, so we have to use it with Python 3.x. In these cases, we cannot build a dropbox but need to rely on the tools described below.

Available parsers:
- Tescan Clara SEM: TIFF image, python 2.7 compatible
- FEI Helios SEM: TIFF image, python 2.7 compatible
- Zeiss Leo SEM: TIFF image, python 2.7 compatible
- EDAX Team Apex software (zip file format): Unfortunately, not a lot of metadata are extractable. The zip archive can also contain many different measurements from many samples, so it's not useful to extract metadata from just one of the measurements.
- [EMSoft](https://github.com/EMsoft-org/EMsoft) Simulation toolkit for EBSD patterns (open-source). The files are based on the [HDF5](https://www.hdfgroup.org/solutions/hdf5/) standard and separate file types are used for Monte-Carlo Simulation, Masterpattern simulation and Screenpattern simulation.
- Gatan Microscopy Suite dm3/dm4 files (TEM, through [RosettaSciIO](https://github.com/hyperspy/rosettasciio))
- Thermo Scientific Velox Software emd files (TEM, through [RosettaSciIO](https://github.com/hyperspy/rosettasciio))  

# Register Link Data
Handle metadata for files that are stored on an external data store (in our case, S3). See the readme in the folder for more details. For convenience, the tool also allows to upload data to openBIS directly.

A streamlit app is also included that allows to upload & download data from openBIS and an S3 storage with a graphical user interface. This has the advantage that metadata can be extracted with Python 3 compatible code and users have one place to register and download data.

## Funding
Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) as part of Collaborative Research Centre CRC 1394 - Structural and Chemical Atomic Complexity - From Defect Phase Diagrams to Material Properties (project number 409476157) and under the National Research Data Infrastructure – NFDI 38/1 (project number 460247524)